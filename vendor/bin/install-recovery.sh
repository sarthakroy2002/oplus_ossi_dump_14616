#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/by-name/recovery:134217728:9ad979997fc583156402eb174a594122fb6edbf0; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/by-name/boot:33554432:bb77e20c08c0577ebc6c48c59b972ebebfd3cdc5 \
          --target EMMC:/dev/block/by-name/recovery:134217728:9ad979997fc583156402eb174a594122fb6edbf0 && \
      log -t recovery "Installing new oplus recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oplus recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
